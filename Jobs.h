#include <iostream>

class Jobs
{
public:
	Jobs(){}
    Jobs(int jID,int jRank,int jexeTime){id=jID; rank=jRank; exeTime=jexeTime;}
	~Jobs(){}
	int getID(){return id;}
	int getRank(){return rank;}	//rank=priority
	int getExeTime(){return exeTime;}
	void setID(int a){id=a;}
	void setRank(int a){rank=a;}
    void setExeTime(int a){exeTime=a;}

    bool operator< (const Jobs& otherJobs)const
    {
        if (rank<otherJobs.rank){return true;}
        else {return false;}
    }
private:
	int id;
	int rank;
	int exeTime;
};
