#include "mainwindow.h"
#include "ui_mainwindow.h"
//#include "Jobs.h"
#include <QTimer>
#include <random>
#include <QTextEdit>
#include <ctime>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    amountOfJobs=0;//initialize amountOfJobs to 0
    inisBValue=0;
    amountOfJobsBQ=0;
    cpuIsActive=false;
    ui->setupUi(this);
    ui->sB_Jobs->setValue(1);
    ui->tE_Status->setText("Total Jobs in Queue: "+QString::number(amountOfJobs));
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(on_pB_Start_clicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_sB_Jobs_valueChanged(int value)
{
    inisBValue=value;
}

void MainWindow::on_pB_New_clicked()
{

    ui->tE_Status->clear();   //Clear text editor on click
    amountOfJobs+=inisBValue;
    ui->sB_Jobs->setValue(1); //Reset spinbox counter to 1 on click
    //On click generate random Job ID numbers based on the spinner amount
    std::default_random_engine seed(time(nullptr));
    std::uniform_int_distribution<int> iJobID(0,20);
    std::uniform_int_distribution<int> iJobRank(20,99);
    std::uniform_int_distribution<int> iexeTime(1000,5000);

    ui->tE_Status->append("Jobs in Ready Queue: "+QString::number(amountOfJobs));

    if (amountOfJobs == 0){ui->tE_Status->append("No Jobs created");}
    else
    {
        //Pass in Jobs objects into ready queue
        if (!readyQueue.empty())
        {
            //Clear readyQueue
            unsigned rQSize = readyQueue.size();
            for (unsigned int i=0; i<rQSize; i++){readyQueue.pop();}

            //Fill readyQueue with Job objects
            for (int i=0; i<amountOfJobs; i++){
                //ui->tE_Status->append("Size of Ready Queue: "+QString::number(readyQueue.size())); //TEST
                readyQueue.emplace(iJobID(seed),iJobRank(seed),iexeTime(seed));
            }
        }
        else
        {
            //Fill readyQueue with Job objects
            for (int i=0; i<amountOfJobs; i++){readyQueue.emplace(iJobID(seed),iJobRank(seed),iexeTime(seed));}
        }

        dReadyQueue=readyQueue; //Copy readyQueue contents into dReadyQueue

        //Update next in Queue label
        Jobs a;
        a=readyQueue.top();
        ui->lbl_Job_0->setText("J"+QString::number(a.getID()));

        //Display Jobs in Queue
        for (int i = 0; i<amountOfJobs;i++){
            a = dReadyQueue.top();
            ui->tE_Status->append("Job#"+QString::number(i+1)+"\n"
                                  "Job ID: "+QString::number(a.getID())+"\n"+
                                  "Job Rank: "+QString::number(a.getRank())+"\n"+
                                  "Job exe. Time: "+QString::number(a.getExeTime())+"\n");
            dReadyQueue.pop();
        }

        ui->pB_Start->setEnabled(true); //Enable start button
        dReadyQueue = readyQueue;
        dReadyQueue.pop();  //Pop to show next job in queue
        //ui->tE_Status->append("Size of dReady Queue: "+QString::number(dReadyQueue.size())); //TEST
        ui->tE_Status->append("--------------------------\n");
    }

}

void MainWindow::on_pB_Start_clicked()
{
    Jobs a,b,c;
    cpuIsActive = true;
    timer->start(TIMEVAL); //TIMEVAL= 3 seconds
    ui->lbl_Timer->setText(QString::number(timer->remainingTime()/1000)+" s");

    if (!dReadyQueue.empty())       //Pop contents out of dReadyQueue
    {
        b = dReadyQueue.top();
        dReadyQueue.pop();
        ui->lbl_Job_0->setText("J"+QString::number(b.getID()));
    }
    else {ui->lbl_Job_0->setText(" ");}

    if (!blockQueue.empty())        //Pop contents out of blockQueue back into readyQueue
    {
        std::default_random_engine seed(time(nullptr));
        std::uniform_int_distribution<int> iJobRank(0,40);
        c = blockQueue.top();
        c.setRank(iJobRank(seed)); //set new rank
        ui->lbl_Block_0->setText("J"+QString::number(c.getID()));
        ui->tE_Status2->append("JobID "+QString::number(c.getID())+" moved to Ready Queue");
        ui->tE_Status2->append("JobID "+QString::number(c.getID())+
                               " now Rank "+QString::number(c.getRank())+"\n");
        dReadyQueue.push(c);
        readyQueue.push(c);
        amountOfJobs++;
        amountOfJobsBQ--; //***Presents inaccuracy of number of jobs in blocked queue*
        ui->tE_Status->append("Jobs in Blocked Queue: "+QString::number(amountOfJobsBQ));
        ui->tE_Status->append("Job ID: "+QString::number(c.getID())+"\n"+
                              "Job Rank: "+QString::number(c.getRank())+"\n"+
                              "Job exe. Time: "+QString::number(c.getExeTime())+"\n");
        blockQueue.pop();
    }
    else {ui->lbl_Block_0->setText(" ");}

    if (!readyQueue.empty())        //Pop contents out of readyQueue
    {
        ui->pB_Terminate->setEnabled(true);
        a = readyQueue.top();
        ui->tE_Status2->append("Jobs in Ready Queue: "+QString::number(amountOfJobs));
        ui->tE_Status2->append("JobID "+QString::number(a.getID())+" being processed\n");
        ui->lbl_CPU->setText("J"+QString::number(a.getID()));

        //Compare exe.Time with TIMEVAL(3000ms)
        //Pop Job into block queue if exe.Time>TIMEVAL
        if (a.getExeTime()>TIMEVAL)
        {
            amountOfJobsBQ++;
            a.setExeTime(a.getExeTime()-TIMEVAL);
            blockQueue.emplace(a.getID(),a.getRank(),a.getExeTime());
            ui->tE_Status2->append("JobID "+QString::number(a.getID())+" moved to Block Queue");
            ui->tE_Status2->append("JobID "+QString::number(a.getID())+" exe. Time is now "+
                                            QString::number(a.getExeTime())+"\n");
            ui->tE_Status->append("Jobs in Blocked Queue: "+QString::number(amountOfJobsBQ));
            ui->tE_Status->append("Job ID: "+QString::number(a.getID())+"\n"+
                                  "Job Rank: "+QString::number(a.getRank())+"\n"+
                                  "Job exe. Time: "+QString::number(a.getExeTime())+"\n");
            readyQueue.pop();
            amountOfJobs--;
        }
        else
        {
            readyQueue.pop();
            amountOfJobs--;
        }

    }
    else
    {
        timer->stop();
        ui->lbl_CPU->setText(" ");
        ui->lbl_Job_0->setText(" ");
        ui->tE_Status2->append("\nAll Jobs Processed!");
        ui->pB_Start->setEnabled(false);
        ui->pB_Terminate->setEnabled(false);
    }
}

void MainWindow::on_pB_Terminate_clicked()
{
    Jobs a;

    //If a Job is being processed (check with bool. condition) time slice and move to blocked queue
    if (amountOfJobs == 0){ui->tE_Status2->append("\nOnly 1 Job is being processed\nCannot Terminate");}
    else if(cpuIsActive && (timer->remainingTime()>0))
    {
        amountOfJobsBQ++;
        a = readyQueue.top();
        if(a.getExeTime()>TIMEVAL){a.setExeTime(a.getExeTime()-TIMEVAL);}
        if(a.getExeTime()<TIMEVAL){a.setExeTime(TIMEVAL-a.getExeTime());}
        blockQueue.emplace(a.getID(),a.getRank(),a.getExeTime());
        ui->tE_Status2->append("\nJobID "+QString::number(a.getID())+" Terminated");
        ui->tE_Status2->append("JobID "+QString::number(a.getID())+" moved to Block Queue");
        ui->tE_Status2->append("JobID "+QString::number(a.getID())+" exe. Time is now "+
                                        QString::number(a.getExeTime())+"\n");
        ui->tE_Status->append("Jobs in Blocked Queue: "+QString::number(amountOfJobsBQ));
        ui->tE_Status->append("Job ID: "+QString::number(a.getID())+"\n"+
                              "Job Rank: "+QString::number(a.getRank())+"\n"+
                              "Job exe. Time: "+QString::number(a.getExeTime())+"\n");
        //readyQueue.pop();
        //amountOfJobs--;
        timer->stop();
        ui->lbl_Job_0->setText(" ");
        ui->lbl_CPU->setText("J"+QString::number(a.getID()));
        ui->tE_Status2->append("\nProcess Interupted!\nPress Start to continue...\n");
        ui->pB_Terminate->setEnabled(false);
    }
}
