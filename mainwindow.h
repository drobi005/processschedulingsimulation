#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <queue>
#include <QTimer>
#include "Jobs.h"
//#include <QTextEdit>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    int amountOfJobs,inisBValue,amountOfJobsBQ;
    bool cpuIsActive;
    const int TIMEVAL = 3000; //3 seconds
    int rTime = 3;
    std::priority_queue<Jobs> readyQueue;
    std::priority_queue<Jobs> blockQueue;
    std::priority_queue<Jobs> dReadyQueue;
    std::priority_queue<Jobs> dBlockQueue;

private slots:

    void on_sB_Jobs_valueChanged(int value);
    void on_pB_New_clicked();
    void on_pB_Start_clicked();

    void on_pB_Terminate_clicked();

private:
    Ui::MainWindow *ui;
    QTimer *timer;
};

#endif // MAINWINDOW_H
